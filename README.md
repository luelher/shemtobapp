== Instalar ==

phonegap plugin add https://github.com/litehelpers/Cordova-sqlite-storage.git

phonegap plugin add org.apache.cordova.file

phonegap plugin add org.apache.cordova.file-transfer

phonegap plugin add org.apache.cordova.media

phonegap plugin add cordova-plugin-statusbar

phonegap plugin add org.apache.cordova.inappbrowser


== Firmar ==

# info: https://developer.android.com/tools/publishing/app-signing.html

# OJO:
# ========================================
# NOTE: Change number of release in file =
# ========================================

# Aumentar el Nro del Relese en el config.xml
# Compilar
phonegap build android -d --release
  
# Copiar el archivo unsigned a la carpeta de dropbox

# Generar el Key
keytool -genkey -v -keystore /home/luelher/Dropbox/android/certificates/luelher.keystore -alias luelher -keyalg RSA -keysize 2048 -validity 10000

# copy unsigned release
cp /home/luelher/Dropbox/android/shemtobApp/android-release-unsigned.apk /home/luelher/Dropbox/android/shemtobApp/android-release-signed.apk

# sign release
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore /home/luelher/Dropbox/android/certificates/luelher.keystore /home/luelher/Dropbox/android/shemtobApp/android-release-signed.apk luelher

# Align release
# NOTE: Change number of release in file
~/android-sdk-linux/build-tools/19.1.0/zipalign -v 4 /home/luelher/Dropbox/android/shemtobApp/android-release-signed.apk /home/luelher/Dropbox/android/shemtobApp/shemtobApp-0.14-release-signed-align.apk
