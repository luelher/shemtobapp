/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();

        var host_server_rest = 'http://rabmaleh.com';

        // create the module and name it shemtopApp
        var shemtopApp = angular.module('shemtopApp', ['ngRoute', 'mobile-angular-ui', 'mobile-angular-ui.components', 'mediaPlayer', 'ui.slider', 'ngCordova']);

        // factories
        shemtopApp.factory('canciones', function() {
            var canciones = {};
            var cancion = 0;
            var cancionesService = {};
            var desde = "";
            
            cancionesService.setCanciones = function(data) {
                canciones = data;
            };
            cancionesService.getCanciones = function() {
                return canciones;
            };

            cancionesService.findCancion = function(id) {
                c = $.grep(canciones, function(e){ return e.id == id; });
                if(c.length==0){
                    return {id: 0};
                }else{
                    return c[0];
                }
            };


            cancionesService.setCancion = function(data) {
                cancion = data;
            };
            cancionesService.getCancion = function() {
                return cancion;
            };

            cancionesService.setDesde = function(data) {
                desde = data;
            };
            cancionesService.getDesde = function() {
                return desde;
            };

            cancionesService.countCanciones = function() {
                return canciones.length;
            };
            
            return cancionesService;
        });


        shemtopApp.factory('albunes', function() {
            var albunes = {};
            var albunesService = {};
            var album = 0;
            var album_title = "";
            var album_img = "";
            
            albunesService.setAlbunes = function(data) {
                albunes = data;
            };
            albunesService.getAlbunes = function() {
                return albunes;
            };

            albunesService.countAlbunes = function() {
                return albunes.length;
            };

            albunesService.findCancion = function(id) {
                can = null;
                angular.forEach(albunes, function (alb) {
                    c = $.grep(alb.canciones, function(e){ return e.id == id; });
                    if(c.length>0){
                        can = c;
                    }
                });
                if(c.length>0){
                    return can[0];
                }else return {id: 0}
                
            };

            albunesService.setAlbum = function(data) {
                album = data;
            };
            albunesService.getAlbum = function() {
                return album;
            };

            albunesService.setAlbumTitle = function(data) {
                album_title = data;
            };
            albunesService.getAlbumTitle = function() {
                return album_title;
            };

            albunesService.setAlbumImg = function(data) {
                album_img = data;
            };
            albunesService.getAlbumImg = function() {
                return album_img;
            };

            return albunesService;
        });


        // routes
        shemtopApp.config(function($sceDelegateProvider, $routeProvider) {

                $routeProvider.when('/busqueda', {
                    templateUrl : 'pages/busqueda.html',
                    controller  : 'busquedaController',
                    reloadOnSearch: false
                });

                $routeProvider.when('/', {
                    templateUrl : 'pages/home.html',
                    controller  : 'homeController',
                    reloadOnSearch: false
                });

                $routeProvider.when('/albums', {
                    templateUrl : 'pages/albums.html',
                    controller  : 'albumsController',
                    reloadOnSearch: false
                });

                $routeProvider.when('/album_detalle', {
                    templateUrl : 'pages/album_detalle.html',
                    controller  : 'albumDetalleController',
                    reloadOnSearch: false
                });

                $routeProvider.when('/novedades', {
                    templateUrl : 'pages/novedades.html',
                    controller  : 'novedadesController',
                    reloadOnSearch: false
                });

                $routeProvider.when('/audios', {
                    templateUrl : 'pages/audios.html',
                    controller  : 'audiosController',
                    reloadOnSearch: false
                });

                $routeProvider.when('/donar', {
                    templateUrl : 'pages/donaciones.html',
                    controller  : 'donacionesController',
                    reloadOnSearch: false
                });

                $routeProvider.when('/play', {
                    templateUrl : 'pages/play.html',
                    controller  : 'playController',
                    reloadOnSearch: false
                });
        });

        shemtopApp.controller('busquedaController', function($scope, $http, $location, $log, canciones) {
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
            $scope.buscar = {};
            $scope.searching = false;
            $scope.wasSubmitted = false;
            $scope.pagina = 0;
            $scope.custom_height = ((window.innerHeight / 4.0)*2.8).toFixed();

            console.log(window.innerHeight);

            if(canciones.countCanciones() > 0){
                $scope.canciones = canciones.getCanciones();
            }

            $scope.buscar = function(reset) {
                try{
                    $scope.wasSubmitted = true;
                    $scope.searching = true;
                    if(reset==true) $scope.pagina = 0;

                    console.log("antes del post");

                    $http.post(
                        host_server_rest+'/app/listar_canciones_json.php',
                        "nombre="+(!$scope.buscar.valor ? '' : $scope.buscar.valor)+"&limit="+$scope.pagina
                        ).success(function(data) {
                            console.log("success del post");
                            $scope.canciones = data;
                            $scope.searching = false;
                            canciones.setCanciones(data);
                    });                
                    console.log("Despues del post");
                }catch(e){
                    console.log("Error..."+e.message);
                }
            };

            $scope.tocar = function(id) {
                canciones.setCancion(id);
                canciones.setDesde("canciones");
            };

            $scope.next_page = function () {
                $scope.pagina += 10;
                $scope.buscar();
            }

            $scope.previous_page = function () {
                $scope.pagina -= 10;
                $scope.buscar();
            }


        });

        shemtopApp.controller('playController', function($scope, $timeout, $log, $sce, $window, $interval, canciones, albunes, $cordovaFileTransfer, $cordovaFile, $cordovaDialogs, $cordovaSQLite, $cordovaMedia) {


            var db = $cordovaSQLite.openDB("media.db");
            var autoplay = false;

            var src = "";
            $scope.audio_media = null;
            $scope.play = false;

            $scope.currentTime = "00:00:00";
            $scope.totalTime = "00:00:00";
            $scope.volume = 0.5;
            $scope.cancel_download = false;


            $scope.VolumenUp = function() {
                if($scope.volume<1.0) $scope.volume += 0.1;
                $scope.audio_media.setVolume($scope.volume);
                console.log("VolumenUp");
                $scope.reproductor.volumen_slider = ($scope.volume * 100);
            }

            $scope.VolumenDown = function() {
                if($scope.volume>0.0) $scope.volume -= 0.1;
                $scope.audio_media.setVolume($scope.volume);
                console.log("VolumenDown");
                $scope.reproductor.volumen_slider = ($scope.volume * 100);
            }

            $scope.CancelDownload = function() {
                $scope.cancel_download = true;
                $scope.descargando = false;
            }


            desde = canciones.getDesde();
            cancion = canciones.getCancion();
            $scope.descargando = false;
            $scope.downloadProgress = 0;
            if(desde=="albunes"){
                data_cancion = albunes.findCancion(cancion);
                $log.info(data_cancion);
            }else{
                data_cancion = canciones.findCancion(cancion);
            }

            $scope.trustSrc = function(src) {
                return $sce.trustAsResourceUrl(src);
            }


            function setSlideAudio (ev, ui) {
                var value = (((ui.value*$scope.audio_media.media._duration)/100)*1000).toFixed();
                $scope.audio_media.seekTo(value);
                console.log("seek: "+value);
            }


            function insertMedia(data){
                var query = "INSERT INTO medios (album_id, album_name, filename, id, image, nombre_cancion) VALUES (?,?,?,?,?,?)";
                $cordovaSQLite.execute(db, query, [parseInt(data.album_id), data.album_name, data.filename, parseInt(data.id), data.image, data.nombre_cancion]).then(function(res) {
                  console.log("insertId: " + res.insertId);
                }, function (err) {
                  console.error(JSON.stringify(err));
                });
            }


            $scope.DownloadButton = function(){
                
                console.log("Download");

                var query = "CREATE TABLE IF NOT EXISTS medios (album_id INTEGER, album_name TEXT, filename TEXT, id INTEGER, image TEXT, nombre_cancion TEXT, PRIMARY KEY(id))";
                $cordovaSQLite.execute(db, query, []).then(function(res) {
                    console.error('Tabla Creada');
                }, function (err) {
                    console.error(err);
                });

                $cordovaFile.checkFile(cordova.file.dataDirectory, $scope.id + ".mp3")
                    .then(function (success) {

                        $cordovaDialogs.alert('El archivo ya fue descargado', 'shemtopApp', 'Aceptar');

                    }, function (error) {
                        if(!$scope.descargando){

                            if($scope.filename!=""){
                                console.log("Tratando...");
                                var targetPath = cordova.file.dataDirectory + $scope.id + ".mp3";
                                var trustHosts = true
                                var options = {};

                                $scope.downloadObj = $cordovaFileTransfer.download($scope.filename, targetPath, options, trustHosts);
                                $scope.descargando = true;
                                $scope.downloadObj
                                    .then(function(result) {
                                        data = {
                                            album_id: $scope.album_id,
                                            album_name: $scope.album_name,
                                            filename: targetPath,
                                            id: $scope.id,
                                            image: $scope.image,
                                            nombre_cancion: $scope.nombre_cancion
                                        };
                                        insertMedia(data);
                                        console.log("Guardado en tabla SQLite: "+JSON.stringify(result));
                                    }, function(err) {
                                        console.log("Error"+JSON.stringify(err));
                                    }, function (progress) {
                                        $timeout(function () {
                                          $scope.downloadProgress = ((progress.loaded / progress.total) * 100).toFixed(1);
                                          //console.log("Descargando..."+$scope.downloadProgress+"%");
                                          //$scope.descargando = true;
                                        });
                                        if($scope.cancel_download){
                                            $scope.downloadObj.abort();
                                            $scope.cancel_download = false;
                                            console.log("Descarga Cancelada");
                                        }
                                    });
                                
                            }else{
                                $cordovaDialogs.alert('El archivo ya fue descargado', 'shemtopApp', 'Aceptar');
                                console.log("Falló");
                            }
                        }else{
                          $cordovaDialogs.alert('Ya se está descargando un archivo. Espere por favor', 'shemtopApp', 'Aceptar');
                        }
                    });                
            }

            $scope.BackButton = function(){
                $window.history.back();
            }

            if(data_cancion.id==0){
                $scope.album_id = "";
                $scope.album_name = "";
                $scope.album_url = "";
                $scope.filename = "";
                $scope.id = "";
                $scope.image = "";
                $scope.nombre_cancion = "";
            }else{
                $scope.album_id = data_cancion.album_id;
                $scope.album_name = data_cancion.album_name;
                $scope.album_url = "";
                $scope.filename = data_cancion.filename;
                $scope.id = data_cancion.id;
                $scope.image = data_cancion.image;
                $scope.nombre_cancion = data_cancion.nombre_cancion;
            }

            $scope.defaultPlaylist = [{ src: $scope.filename, type: 'audio/mpeg' }];

            $scope.reproductor = {
                time_slider: 0,
                volumen_slider: 50,
                options_audio: {
                    orientation: 'horizontal',
                    min: 0,
                    max: 100,
                    range: 'min',
                    slide: setSlideAudio,
                    seekable: true
                },
                options_volumen: {
                    orientation: 'horizontal',
                    min: 0,
                    max: 100,
                    range: 'min',
                    seekable: false
                }

            };

            $scope.PlayPauseButton = function () {
                console.log("Play Toggle");
                if($scope.audio_media==null && $scope.play==false){
                    $scope.audio_media = $cordovaMedia.newMedia($scope.filename);
                    $scope.audio_media.play();
                    $scope.audio_media.setVolume($scope.volume);
                    $scope.play = true;           

                    console.log("Cargado y Play");
                }else{
                    if(!$scope.play){
                        $scope.audio_media.play();
                        $scope.play = true;
                        console.log("play");
                    }else{
                        $scope.audio_media.pause();
                        $scope.play = false;
                        console.log("pause");
                    }
                }
            };


            function updateSlide(currentTime) {
                if(currentTime > 0.0){
                    $scope.reproductor.time_slider = (currentTime*100/$scope.audio_media.media._duration).toFixed()
                    $scope.currentTime = toHMS(currentTime);
                    $scope.totalTime = toHMS($scope.audio_media.media._duration);

                }else{
                    if($scope.audio_media==null && !autoplay){
                        autoplay = true;
                        $scope.PlayPauseButton();                        
                    }
                }
            }


            var mediaTimer = $interval(function () {
                // get media position
                if($scope.audio_media!=null){
                    $scope.audio_media.media.getCurrentPosition(
                        // success callback
                        function (position) {
                            if (position > -1) {
                                updateSlide(position);
                            }
                        },
                        // error callback
                        function (e) {
                            console.log("Error getting pos=" + e);
                        }
                    );                    
                }else {
                    updateSlide(0.0);
                }
            }, 1000);

            $scope.$on('$destroy', function () { $interval.cancel(mediaTimer); $scope.audio_media.media.release(); });

            function toHMS(seconds){
                var date = new Date(null);
                date.setSeconds(seconds); // specify value for SECONDS here
                return date.toISOString().substr(11, 8);
            }

        });        


        shemtopApp.controller('albumsController', function($scope, $sce, $http, albunes, canciones) {
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
            $scope.albunes = {};
            $scope.searching = false;
            $scope.wasSubmitted = false;
            $scope.buscar = {valor: ""};
            $scope.custom_height = ((window.innerHeight / 4.0)*2.8).toFixed();

            if(albunes.countAlbunes() > 0){
                $scope.albunes = albunes.getAlbunes();
            }

            $scope.trustSrc = function(src) {
                return $sce.trustAsResourceUrl(src);
            }

            $scope.buscar = function() {
                $scope.wasSubmitted = true;
                $scope.searching = true;

                $http.post(
                    host_server_rest+'/app/listar_album_json.php',
                    "nombre="+(!$scope.buscar.valor ? '' : $scope.buscar.valor)
                    ).success(function(data) {
                        $scope.albunes = data;
                        $scope.searching = false;
                        albunes.setAlbunes(data);
                        // angular.forEach(data, function(alb, index){
                        //     $scope.albunes[index].canciones = [];
                        //     $scope.albunes[index].cantidad = 0;
                        //     $scope.albunes[index].cargando_canciones = true;
                        //     $http.post(
                        //         host_server_rest+'/app/listar_canciones_album_json.php',
                        //         "id="+alb.id
                        //         ).success(function(data) {
                        //             $scope.albunes[index].canciones = data;
                        //             $scope.albunes[index].cantidad = data.length;
                        //             albunes.setAlbunes($scope.albunes);
                        //             $scope.albunes[index].cargando_canciones = false;
                        //         });
                        // });
                });                
            };

            $scope.mostrar = function(id, title) {
                albunes.setAlbum(id);
                albunes.setAlbumTitle(title);
            };

            $scope.buscar();

        });


        shemtopApp.controller('albumDetalleController', function($scope, $http, $location, $log, canciones, albunes) {
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
            $scope.buscar = {};
            $scope.pagina = 0;
            $scope.custom_height = ((window.innerHeight / 4.0)*3.0).toFixed();
            $scope.album = albunes.getAlbum();
            $scope.title = albunes.getAlbumTitle();

            $scope.buscar = function(reset) {
                try{
                    $scope.wasSubmitted = true;
                    $scope.searching = true;
                    if(reset==true) $scope.pagina = 0;

                    console.log("antes del post");

                    $http.post(
                        host_server_rest+'/app/listar_canciones_album_json.php',
                        "id="+(!$scope.album ? '' : $scope.album)+"&limit="+$scope.pagina
                        ).success(function(data) {
                            console.log("success del post");
                            $scope.canciones = data;
                            $scope.searching = false;
                            canciones.setCanciones(data);
                    });                
                    console.log("Despues del post");
                }catch(e){
                    console.log("Error..."+e.message);
                }
            };

            $scope.tocar = function(id) {
                canciones.setCancion(id);
                canciones.setDesde("canciones");
            };

            $scope.next_page = function () {
                $scope.pagina += 10;
                $scope.buscar();
            }

            $scope.previous_page = function () {
                $scope.pagina -= 10;
                $scope.buscar();
            }

            $scope.buscar();

        });



        shemtopApp.controller('novedadesController', function($scope, $http) {

            $scope.load = false;

            $http.get(host_server_rest+'/app/listar_novedades_json.php')
                .success(function(data) {
                    $scope.novedades = data;
                    $scope.load = true;
                    // angular.forEach(data, function(novedad, index){
                    //     $scope.novedades[index].full = '<img src="img/loading.gif" alt="">';
                    //     $http.post(
                    //         host_server_rest+'/app/traer_novedad_json.php',
                    //         "id="+novedad.id
                    //         ).success(function(data) {
                    //             if(data[0].intro!='')
                    //                 $scope.novedades[index].full = data[0].intro;
                    //             else
                    //                 $scope.novedades[index].full = data[0].full;
                    //         });
                    // });
            });
        });

        shemtopApp.controller('audiosController', function($scope, canciones, $cordovaDialogs, $cordovaSQLite, $cordovaFile) {
            $scope.descargados = [];
            var db = $cordovaSQLite.openDB("media.db");

            $scope.custom_height = ((screen.availHeight / 3) * 2).toFixed();

            var query = "CREATE TABLE IF NOT EXISTS medios (album_id INTEGER, album_name TEXT, filename TEXT, id INTEGER, image TEXT, nombre_cancion TEXT, PRIMARY KEY(id))";
            $cordovaSQLite.execute(db, query, []).then(function(res) {
                console.log('Tabla Creada');
            }, function (err) {
                console.error(err);
            });

            
            $scope.readMedia = function() {

            };
            
            $scope.deleteMedia = function(id){

                console.log("deleteMedia");


                  $cordovaDialogs.confirm('¿Desea eliminar el archivo del equipo?', 'Eliminar Archivo')
                    .then(function(buttonIndex) {
                      // Sí = 0, 'No' = 1, 'Cancel' = 2
                      console.log("Respuesta: "+buttonIndex);

                      if(buttonIndex==1){

                        console.log("ACeptar");
                        var query = "DELETE FROM medios where id="+id+";";
                        console.log("SQL="+query);
                        $cordovaSQLite.execute(db, query, []).then(function(res) {

                            console.log("En El Exceute");
                            $cordovaFile.removeFile(cordova.file.dataDirectory, id + ".mp3")
                              .then(function (success) {
                                console.log("Eliminado");
                                $scope.descargados = [];
                                $scope.getFiles();
                              }, function (error) {
                                console.log(error);
                              });

                        }, function (err) {
                          console.error(err);
                        });



                      }
                    });




            }
            
            $scope.getFiles = function() {
                console.log("GetFiles");

                var query = "SELECT * FROM medios;";
                $cordovaSQLite.execute(db, query, []).then(function(res) {
                  
                  console.log("Contador: " + res.rows.length);
                  console.log(JSON.stringify(res.rows))

                  if(res.rows.length>0){
                    for(var i = 0; i < res.rows.length; i++){

                        $scope.descargados.push({
                                                album_id: res.rows.item(i).album_id,
                                                album_name: res.rows.item(i).album_name,
                                                filename: res.rows.item(i).filename,
                                                id: res.rows.item(i).id,
                                                image: res.rows.item(i).image,
                                                nombre_cancion: res.rows.item(i).nombre_cancion
                                            });
                    };
                  }

                }, function (err) {
                  console.error(err);
                });

            };
                
            
            $scope.deleteFile = function(path) {
            };

            $scope.tocar = function(obj) {
                canciones.setCancion(obj.id);
                canciones.setDesde("busqueda");
                data = [obj];
                canciones.setCanciones(data);
            };
            
            $scope.getFiles();

        });

        shemtopApp.controller('donacionesController', function($scope, $http) {
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

            $scope.load = false;

            $http.post(host_server_rest+'/app/traer_novedad_json.php','id=62')
                .success(function(data) {
                    $scope.donaciones = data;
                    $scope.load = true;
            });

        });

        shemtopApp.controller('homeController', function($scope, $cordovaFile, $cordovaDialogs, $cordovaSQLite) {
            $scope.message = '';

            $scope.pagina = function(){
                window.open(host_server_rest, '_system');
            }
        });

        shemtopApp.filter('unsafe', function($sce) {
            return function(val) {
                return $sce.trustAsHtml(val);
            };
        });


    },
    
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('load', this.onLoad, false);
        document.addEventListener('deviceready', this.onDeviceReady, false);
        // window.addEventListener("orientationchange", orientationChange, true);
    },
    onLoad: function() {
        
    },
   
    // deviceready Event Handler
    onDeviceReady: function() {
        console.log("DeviceReady");

    }
};


